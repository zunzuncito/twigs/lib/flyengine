
import { BSON } from 'bson';

import Cluster from './cluster/index.mjs'

export default class World {
  constructor(data, cfg) {
    this.clusters = [];
    console.log(data);
    console.log(cfg);



  }

  static async construct(wurl, cfg) {
    try {
      const world_res = await fetch(wurl);
      const world_uint8 = new Uint8Array(await world_res.arrayBuffer());
      const world_data = BSON.deserialize(world_uint8);

      const _this = new World(world_data, cfg);
      cfg.lods = [1, ...world_data.lods];
      cfg.c_width = (world_data.cluster_width-1)*cfg.tile_size;
      cfg.c_height = (world_data.cluster_height-1)*cfg.tile_size;
      cfg.width = world_data.width;
      cfg.height = world_data.height;

      cfg.total_width = cfg.width * cfg.c_width;
      cfg.total_height = cfg.height * cfg.c_height;


      for (let cluster of world_data.clusters) {
        _this.clusters.push(await Cluster.construct(cluster, cfg));
        console.log(cluster);
      }

      return _this;
    } catch (e) {
      console.error(e);
    }
  }

}
