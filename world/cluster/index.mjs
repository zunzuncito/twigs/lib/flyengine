
import { BSON } from 'bson';

import Terrain from './terrain.mjs';


import * as THREE from 'three';


export default class Cluster {
  constructor(terrain_lods, data, cfg) {

    this.lod = new THREE.LOD();
    this.lod.name = cfg.name;
    for (let t = 0; t < terrain_lods.length; t++) {
      this.lod.addLevel(terrain_lods[t].mesh, cfg.lod_distances[t]);
    }

    console.log("pos x", data.x * cfg.c_width - cfg.total_width / 2);
    console.log("pos z", data.y * cfg.c_height - cfg.total_height/ 2);
    this.lod.position.x = data.x * cfg.c_width - cfg.total_width / 2;
    this.lod.position.z = data.y * cfg.c_height - cfg.total_height/ 2;
		this.lod.updateMatrix();
  }

  static async construct(cluster_data, cfg) {
    try {
      const terrain_lods = [];

      console.log(cluster_data);

      cfg.name = `cluster-${cluster_data.x}-${cluster_data.y}`;
      for (let l = 0; l < cluster_data.lods.length; l++) {
        const lod = cluster_data.lods[l];
        const lod_res = await fetch("world/"+lod.name);
        const lod_uint8 = new Uint8Array(await lod_res.arrayBuffer());
        const lod_data = BSON.deserialize(lod_uint8);
        terrain_lods.push(new Terrain(lod_data, cfg, l));
      }


      const _this = new Cluster(terrain_lods, cluster_data, cfg);


      return _this;
    } catch (e) {
      console.error(e);
    }
  }

}
