
import FlyThree from 'zunzun/flythree/index.mjs';
const THREE = FlyThree.THREE;

export default class Terrain {

  constructor(data, cfg, lod_index) {
    console.log(data);
    this.width = data.width;
    this.height = data.height;


    const vertex_count = (data.width - 1) * (data.height - 1) * 6;
    const colors = new Float32Array(vertex_count * 3);
    for (let v = 0; v < vertex_count * 3; v++) {
      colors[v] = Math.random();
    }
    

    this.buffer_geometry = new FlyThree.BufferGeometry({
      position: {
        data: FlyThree.HeightMap.vertices(data.width, data.height, cfg.tile_size/cfg.lods[lod_index], cfg.height_mult, data.heights.buffer),
        dimensions: 3
      },
      color: {
        data: colors,
        dimensions: 3
      }
    });

    this.material = new THREE.MeshBasicMaterial({ 
      side: THREE.DoubleSide,
			vertexColors: true
    });
    this.material.side = THREE.DoubleSide
    this.buffer_geometry.geometry.computeVertexNormals()

    this.mesh = new THREE.Mesh(this.buffer_geometry.geometry, this.material);
    this.mesh.name = `${cfg.name}_lod-${this.width}-${this.height}`;
  }
  
}
